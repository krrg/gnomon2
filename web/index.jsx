'use strict'

require("./index.scss")
require('normalize.css')

import React from 'react';
import ReactDOM from 'react-dom';

import {Card, CardMedia, CardTitle} from 'material-ui/Card';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import {fade} from 'material-ui/utils/colorManipulator';
import {
	white,
	darkBlack,
	fullBlack,
	blueGrey900,
	blueGrey700,
	blueGrey500,
	deepOrangeA700,
	teal700,
	grey500,
	teal200,
	grey300,
		 } from 'material-ui/styles/colors';

import TopBar from './components/common/topbar';

const gnomonTheme = getMuiTheme({
	palette: {
		primary1Color: blueGrey900,
		primary2Color: blueGrey700,
		primary3Color: blueGrey500,
		accent1Color: deepOrangeA700,
		accent2Color: teal700,
		accent3Color: grey500,
		textColor: white,
		alternateTextColor: white,
		canvasColor: teal200,
		borderColor: grey300,
		disabledColor: fade(darkBlack, 0.3),
		pickerHeaderColor: blueGrey700,
		clockCircleColor: fade(darkBlack, 0.07),
		shadowColor: fullBlack,
	}
})


export class App extends React.Component {



	renderTopMenu() {
		return <TopBar />
	}

	renderMainSplash() {
		return (
			<Card>
				<CardMedia overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle" />}>
			      <img src={require('./img/gnomon_1920x1080.jpg')} />
			    </CardMedia>
			</Card>
		)
	}

	renderCopyright() {
		return (
			<p>Copyright / Footer </p>
		)
	}


	render() {
		return (
			<MuiThemeProvider muiTheme={gnomonTheme}>
				<div>
					{this.renderTopMenu()}
					{this.renderMainSplash()}
					{this.renderCopyright()}
				</div>
			</MuiThemeProvider>


		);
	}
}

const injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

ReactDOM.render(<App/>, document.querySelector("#reactApp"));
